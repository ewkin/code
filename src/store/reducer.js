const initialState = {
    inputCode: '',
    code: '7890',
    background: null
}

const reducer = (state = initialState, action) => {

    if (action.type === 'codeDigit') {
        if (action.value === 'cancel') {
            return {...state, inputCode: '', background: null};
        } else {
            return {...state, inputCode: state.inputCode + action.value};
        }
    }
    if (action.type === 'checkCode') {
        if (state.code === state.inputCode) {
            return {...state, background: 'green'};
        } else {
            return {...state, background: 'red'};
        }
    }

    return state;
}

export default reducer;