import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import './CodeHolder.css';

const CodeHolder = () => {

    const dispatch = useDispatch();
    const state = useSelector(state => state);
    const getNumber = event => {
        dispatch({type: 'codeDigit', value: event.currentTarget.value.toString()});
    }

    const checkNumber = () =>{
        dispatch({type: 'checkCode'});
    }

    let code = state.inputCode.replace(/[0123456789]/g, "#");

    if(state.background ==='green'){
        code = state.inputCode;
    }

    return (
        <div className="calculator">
            <div style={{backgroundColor: state.background}} className="output">{code}</div>
            <div className="calculator-buttons">
                <button onClick={getNumber} value='7' className="calc-button">7</button>
                <button onClick={getNumber} value='8' className="calc-button">8</button>
                <button onClick={getNumber} value='9' className="calc-button">9</button>


                <button onClick={getNumber} value='4' className="calc-button">4</button>
                <button onClick={getNumber} value='5' className="calc-button">5</button>
                <button onClick={getNumber} value='6' className="calc-button">6</button>

                <button onClick={getNumber} value='1' className="calc-button">1</button>
                <button onClick={getNumber} value='2' className="calc-button">2</button>
                <button onClick={getNumber} value='3' className="calc-button">3</button>

                <button onClick={getNumber} value='cancel' className="calc-button">X</button>
                <button onClick={getNumber} value='0' className="calc-button">0</button>
                <button onClick={checkNumber} className="calc-button">E</button>
            </div>

        </div>
    );
};

export default CodeHolder;